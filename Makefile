.SUFFIXES: .html .asc .txt .1

HOSTDIR = esr@login.ibiblio.org:/public/html/catb/esr/faqs/

all: practical-python-porting.html

SNIPPETS = $(shell ls *.py) setpython
practical-python-porting.html: practical-python-porting.txt $(SNIPPETS)
	asciidoc practical-python-porting.txt

MANIFEST = practical-python-porting.html $(SNIPPETS)
manifest:
	@echo $(MANIFEST)

LANDING = $(HOSTDIR)/practical-python-porting
upload: practical-python-porting.html
	scp -q $(SNIPPETS) $(LANDING)
	scp -q practical-python-porting.html $(LANDING)/index.html

VERSION=$(shell sed <practical-python-porting.txt -n '/^version /s///p')
version:
	@echo $(VERSION)

tag:
	git tag $(VERSION) && git push && git push --tags

clean: 
	rm -f practical-python-porting.html
